import com.mobilesecuritycard.openmobileapi.SEService;
import com.mobilesecuritycard.openmobileapi.Session;
import com.mobilesecuritycard.openmobileapi.Channel;
import com.mobilesecuritycard.openmobileapi.Reader;


public class TestAppletClass {

    public class SEServiceCallback implements SEService.CallBack {

        public void serviceConnected(SEService service) {
            _service = service;
            performTest();
        }
    }

    private SEService _service = null;
    private Session _session = null;
    final String LOG_TAG = "HelloSmartcard";

    private void performTest() {

        try {
            System.out.println( "creating SEService object");
            SEServiceCallback callback = new SEServiceCallback();
            new SEService(  , callback);

        } catch (SecurityException e) {
            System.out.println( "Binding not allowed, uses-permission org.simalliance.openmobileapi.SMARTCARD?");
        } catch (Exception e) {
            System.out.println( "Exception: " + e.getMessage());
        }

        try {
            System.out.println("Retrieve available readers...");
            Reader[] readers = _service.getReaders();

            if (readers.length < 1)
                return;

            System.out.println( "Create Session from the first reader...");
            Session session = readers[0].openSession();

            System.out.println( "Create logical channel within the session...");
            Channel channel = session.openLogicalChannel(new byte[] {
                    (byte) 0xD2, 0x76, 0x00, 0x01, 0x18, 0x00, 0x02,
                    (byte) 0xFF, 0x49, 0x50, 0x25, (byte) 0x89,
                    (byte) 0xC0, 0x01, (byte) 0x9B, 0x01 });

            System.out.println( "Send HelloWorld APDU command");
            byte[] respApdu = channel.transmit(new byte[] {
                    (byte) 0x90, 0x10, 0x00, 0x00, 0x00 });

            channel.close();

            // Parse response APDU and show text but remove SW1 SW2 first
            byte[] helloStr = new byte[respApdu.length - 2];
            System.arraycopy(respApdu, 0, helloStr, 0, respApdu.length - 2);
            System.out.println(helloStr);
        } catch (Exception e) {
            System.out.println("Error occured:" + e);
            return;
        }
    }



}
